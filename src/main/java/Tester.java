import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Properties;

import dk.itu.tracy.entity.Entity;
import dk.itu.tracy.facde.JsonParserUtill;
import dk.itu.tracy.facde.Neo4JFacade;
/**
 * Testing the entity to Neo capabilities
 * @author sofus
 *
 */
public class Tester {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		ClassLoader classLoader = Tester.class.getClassLoader();
		File neoFile = new File(classLoader.getResource("NEO4J.properties").getFile());
		Properties neoprop = new Properties();
		neoprop.load(new FileReader(neoFile));
		Neo4JFacade neo = new Neo4JFacade(neoprop);
		File jsonFile;
		//Test
		
		final String[] datafiles = { "testWithStringData.json", "testWithArrayData.json", "testWithObjectData.json" ,"testGitLabCommit.json"};
		for (int i = 0; i < datafiles.length; i++) {
			jsonFile = new File(classLoader.getResource(datafiles[i]).getFile());
			String json = new String(Files.readAllBytes(jsonFile.toPath()));
			Entity ent = JsonParserUtill.parseJson(json);
			neo.persistEntity(ent);
		}
	}

}
