##Tracy Framework (ALPHA WORK!)
####This framework consists of three components:
- [Tracy](https://bitbucket.org/sofus/tracy)
This is the lib that is comprised in the REST implementation and in the  AMQP2NEO converter.
Has a Command line interface built in.
- [AMQP2NEO](https://bitbucket.org/sofus/amqp2neo)
converter to get the Json from AMQP into Neo4J.
- [REST2AMQP](https://bitbucket.org/sofus/amqp2neo)
Endpoint for REST-services like Gitlab and Jira WebHooks.

This framework is part of the [thesis](https://bitbucket.org/sofus/thesis) efficient traceability, conducted by [Sofus Albertsen](https://bitbucket.org/sofus/)